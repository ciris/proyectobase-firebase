"use strict";
module.exports = hideAction;
hideAction.$inject = [ "$auth" ];

function hideAction( $auth ) {
  return {
    restrict: "EA",
    require: "?ngModel",
    scope: true,
    link: link( $auth )
  };
}

function link( $auth ) {
  return function( scope, element, attrs ) {
    scope.permission = attrs.permission;
    scope.action = attrs.action;
    try {
      if ( !$auth.getPayload().permissions[scope.permission][scope.action] ) {
        element.hide();
      }
    }
    catch ( err ) {
      element.hide();
    }
  };
}
