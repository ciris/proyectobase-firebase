"use strict";

module.exports = Validations;

Validations.$inject = [ "$auth" ];

function Validations( $auth ) {
  Validations.letPass = letPass;
  return Validations;

  function letPass( params, permissionsName ) {
    var permissions = $auth.getPayload().permissions[permissionsName];
    if ( !params.id && permissions.create ) {
      return true;
    }
    if ( params.id && params.edit === "true" && permissions.edit ) {
      return true;
    }
    if ( params.id && ( !params.edit || params.edit === "false" ) && permissions.read ) {
      return true;
    }
    return false;
  }
}
