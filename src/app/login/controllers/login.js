"use strict";

module.exports = LoginCtrl;
var User = require( "../models/user.js" );

LoginCtrl.$inject = [ "$auth", "Notifications", "$state", "$sessionStorage", "$localStorage",
"UserAPI" ];
function LoginCtrl( $auth, Notifications, $state, $sessionStorage, $localStorage, UserAPI ) {
  var vm = this;
  vm.login = login;
  vm.user = {
    remember: true
  };

  function login( form, user ) {
    if ( form.$valid ) {
      UserAPI.login( user.login, user.password )( result );
    } else {
      return Notifications.addCustom( 400, "Required information missing " );
    }
  }

  function result( error, data ) {
    if ( error ) {
      Notifications.addCustom( 400, "Invalid Credentials" );
    } else {
      var user = User.loadObj( {
        email: vm.user.login,
        token: data.token
      } );
      $sessionStorage.user = user;
      $auth.setToken( user.token );
      if ( vm.user.remember === true ) {
        $localStorage.user = user;
      }
      Notifications.addCustom( 200, "Welcome " + user.email );
      $state.go( "index.main" );
    }
  }
}
