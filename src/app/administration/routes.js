"use strict";

module.exports = routes;

routes.$inject = [ "$stateProvider" ];
function routes( $stateProvider ) {

  $stateProvider.state( "index.administration", {
    templateUrl: "administration/views/index.html",
    url: "/administration",
    controller: "AdministrationCtrl",
    controllerAs: "mod",
    data: {
      title: "routes.admin.module.title",
      icon: "fa-cog",
      menu: "routes.admin.module.menu"
    }
  } );

  $stateProvider.state( "index.administration.user-list", {
    templateUrl: "administration/views/listUser.html",
    url: "/user?page&qty",
    controller: "ListUserCtrl",
    controllerAs: "vm",
    resolve: {
      list: listUser
    },
    data: {
      title: "routes.admin.user-list",
      icono: "fa-list",
      menu: "routes.admin.user-list"
    }
  } );

  $stateProvider.state( "index.administration.user-one", {
    templateUrl: "administration/views/formUser.html",
    url: "/user/:id?edit",
    controller: "FormUserCtrl",
    controllerAs: "vm",
    resolve: {
      user: getUser
    },
    data: {
      title: "routes.admin.user-form",
      icono: "fa-file-o",
      menu: "routes.admin.user-form"
    }
  } );

  $stateProvider.state( "index.administration.role-list", {
    templateUrl: "administration/views/listRole.html",
    url: "/role?page&qty",
    controller: "ListRoleCtrl",
    controllerAs: "vm",
    resolve: {
      list: listRole
    },
    data: {
      title: "routes.admin.role-list",
      icono: "fa-list",
      menu: "routes.admin.role-list"
    }
  } );

  $stateProvider.state( "index.administration.role-one", {
    templateUrl: "administration/views/formRole.html",
    url: "/role/:id?edit",
    controller: "FormRoleCtrl",
    controllerAs: "vm",
    resolve: {
      role: getRole
    },
    data: {
      title: "routes.admin.role-form",
      icono: "fa-file-o",
      menu: "routes.admin.role-form"
    }
  } );

  listUser.$inject = [ "UserAPI", "$stateParams", "$auth", "$q" ];
  function listUser( UserAPI, $stateParams, $auth, $q ) {
    if ( $auth.getPayload().permissions.user.list ) {
      return UserAPI.list( $stateParams.page, $stateParams.qty );
    }
    return $q.reject( {authenticated: false} );
  }

  getUser.$inject = [ "UserAPI", "$stateParams", "Validations", "$q" ];
  function getUser( UserAPI, $stateParams, Validations, $q ) {
    if ( Validations.letPass( $stateParams, "user" ) ) {
      return UserAPI.get( $stateParams.id, $stateParams.edit );
    }
    return $q.reject( {authenticated: false} );
  }

  listRole.$inject = [ "RoleAPI", "$stateParams", "$auth", "$q" ];
  function listRole( RoleAPI, $stateParams, $auth, $q ) {
    if ( $auth.getPayload().permissions.role.list ) {
      return RoleAPI.list( $stateParams.page, $stateParams.qty );
    }
    return $q.reject( {authenticated: false} );
  }

  getRole.$inject = [ "RoleAPI", "$stateParams", "Validations", "$q" ];
  function getRole( RoleAPI, $stateParams, Validations, $q ) {
    if ( Validations.letPass( $stateParams, "role" ) ) {
      return RoleAPI.get( $stateParams.id, $stateParams.edit );
    }
    return $q.reject( {authenticated: false} );
  }

}
