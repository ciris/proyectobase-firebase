"use strict";

module.exports = Role;

function Role( name, description, permissions, $id ) {
  this.name = name || null;
  this.description = description || null;
  this.permissions = permissions || {};
  this.$id = $id;
}

Role.loadObj = loadObj;

function loadObj( json ) {
  if ( json ) {
    if ( _.isArray( json ) ) {
      return _.map( json, function( elem ) {
        return instance( elem );
      } );
    } else {
      return instance( json );
    }
  }
}

function instance( json ) {
  return new Role( json.name, json.description, json.permissions, json.$id );
}
