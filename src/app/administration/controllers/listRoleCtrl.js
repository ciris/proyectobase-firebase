"use strict";

module.exports = ListRoleCtrl;

ListRoleCtrl.$inject = [ "list", "RoleAPI", "$state", "$stateParams" ];
function ListRoleCtrl( list, RoleAPI, $state, $stateParams ) {
  var vm = this;
  vm.page = parseInt( $stateParams.page || 0 ) + 1;
  vm.qty = parseInt( $stateParams.qty || 10 );
  vm.list = list;
  vm.remove = remove;
  vm.updatePage = updatePage;

  function remove( role ) {
    if ( confirm( "Are you sure you want to remove this Role?" ) ) {
      RoleAPI.remove( role.$id ).then( function() {
        vm.list.docs = _.reject( vm.list.docs, function( elem ) {
          return elem.$id === role.$id;
        } );
        vm.list.counter -= 1;
      } );
    }
  }

  function updatePage( page ) {
    $state.go( $state.current, {page: page, qty: vm.qty}, {reload: false} );
  }
}
