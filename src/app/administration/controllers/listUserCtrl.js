"use strict";

module.exports = ListUserCtrl;

ListUserCtrl.$inject = [ "list", "UserAPI", "$state", "$stateParams", "$auth" ];
function ListUserCtrl( list, UserAPI, $state, $stateParams, $auth ) {
  var vm = this;
  vm.loggedUserId = $auth.getPayload().sub;
  vm.page = parseInt( $stateParams.page || 0 ) + 1;
  vm.qty = parseInt( $stateParams.qty || 10 );
  vm.list = list;
  vm.remove = remove;
  vm.updatePage = updatePage;

  function remove( user ) {
    if ( confirm( "Are you sure you want to remove this User?" ) ) {
      UserAPI.remove( user.$id ).then( function() {
        vm.list.docs = _.reject( vm.list.docs, function( elem ) {
          return elem.$id === user.$id;
        } );
        vm.list.counter -= 1;
      } );
    }
  }

  function updatePage( page ) {
    $state.go( $state.current, {page: page, qty: vm.qty}, {reload: false} );
  }
}
