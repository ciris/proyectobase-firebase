"use strict";

var mod = angular.module( "BaseProject.Main", [] );
mod.config( require( "./routes" ) );

mod.controller( "MainCtrl", require( "./controllers/mainCtrl.js" ) );

module.exports = mod;
