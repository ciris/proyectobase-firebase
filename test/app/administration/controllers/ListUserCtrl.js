"use strict";

var _name = require( "../../../../package.json" ).name;

describe( "ListUserCtrl Test", function() {

  beforeEach( angular.mock.module( _name ) );

  describe( "Initial parameters", function() {
    it( "Should assign a value to the scope", angular.mock.inject( assignValue ) );
  } );

} );

var auth = {
  getPayload: function() {
    return {};
  }
};

function assignValue( $controller ) {
  var value = {docs: [], counter: 0};
  var ctrl = $controller( "ListUserCtrl", { list: value, $auth: auth } );
  expect( ctrl.list.docs ).toEqual( value.docs );
  expect( ctrl.page ).toEqual( 1 );
  expect( ctrl.qty ).toEqual( 10 );
}
