"use strict";

var _name = require( "../../../../package.json" ).name;

describe( "FormRoleCtrl test", function() {

  beforeEach( angular.mock.module( _name ) );

  describe( "Initial parameters", function() {
    it( "Should assign a value to the scope", angular.mock.inject( assignValue ) );
    it( "Should be editing", angular.mock.inject( edit ) );
    it( "Should't be editing", angular.mock.inject( read ) );
  } );

} );

function assignValue( $controller ) {
  var value = {name: "Test"};
  var ctrl = $controller( "FormRoleCtrl", {role: value } );
  expect( ctrl.role.name ).toEqual( value.name );
  expect( ctrl.role.editing ).toEqual( false );
}

function edit( $controller ) {
  var ctrl = $controller( "FormRoleCtrl", {role: {}, $stateParams: {edit: "true"}} );
  expect( ctrl.role.editing ).toEqual( true );
}

function read( $controller ) {
  var ctrl = $controller( "FormRoleCtrl", {role: {}, $stateParams: {edit: "false"}} );
  expect( ctrl.role.editing ).toEqual( false );
}
